Meteor.publish 'events', (collection, id) ->
 Events.find({userId: @userId})

Meteor.publish 'foci', (collection, id) ->
 Foci.find({userId: @userId})

# Meteor.publish 'relations', (collection, id) ->
#  Relations.find({})

Meteor.publish 'googleCalendars', (collection, id) ->
 GoogleCalendars.find({userId: @userId})

Meteor.publish 'tSchemas', (collection, id) ->
 tSchemas.find({})

Meteor.publish 'allUsers', (collection, id) ->
 Meteor.users.find({}, {fields: {"registered_emails": 1, "roles": 1}})
