Meteor.methods
 'tymtu-google-calendar': () ->
  console.log "Meteor.call('tymtu-google-calendar')"

  gc = GoogleCalendars.findOne()

  if gc?.nextSyncToken
   calendars = GoogleApi.get('calendar/v3/users/me/calendarList', {nextSyncToken: gc.nextSyncToken})
  else
   calendars = GoogleApi.get('calendar/v3/users/me/calendarList')

  calendars.items = calendars.items.map (e) ->

   start = moment().toDate().toISOString()

   if e.nextSyncToken
    apiCall = "calendar/v3/calendars/#{encodeURIComponent(e.id)}/events?singleEvents=false&timeMin=#{start}&pageToken=#{encodeURIComponent(e.nextSyncToken)}"
   else
    apiCall = "calendar/v3/calendars/#{encodeURIComponent(e.id)}/events?singleEvents=false&timeMin=#{start}"

   GoogleApi.get(apiCall).items.forEach (eventElement) ->

    if eventElement.id == "af0q315078oga4a5e2mka9d46s"
      console.log JSON.stringify(eventElement, null, 2)

    gEventOpts = {
     gId: eventElement.id
     recurringId: eventElement.recurringEventId,
     displayName: eventElement.summary || "--- no summary ---"
     tBlob: {}
     recurrence: eventElement.recurrence
     schema: {'foo': 'bar'}
    }

    # # Preprocess the dates and datetimes into a JSON date
    # if eventElement.recurrence
    #  gEventOpts.startTime = new Date(eventElement.created)
    #  gEventOpts.endTime = moment().set('year', 2020).toDate()
    # else
    if eventElement.start
     if eventElement.start.date
      gEventOpts.startTime = new Date(eventElement.start.date)
      gEventOpts.allDay = true
     else if eventElement.start.dateTime
      gEventOpts.startTime = new Date(eventElement.start.dateTime)
      gEventOpts.allDay = false

    if eventElement.end
     if eventElement.end.date
      gEventOpts.endTime = new Date(eventElement.end.date)
      gEventOpts.allDay = true
     else if eventElement.end.dateTime
      gEventOpts.endTime = new Date(eventElement.end.dateTime)
      gEventOpts.allDay = false


    # gEventOpts.stash = eventElement

    Events.upsert {gId: gEventOpts.gId}, {$set: gEventOpts }

  if gc?
   GoogleCalendars.upsert
    etag: calendars.etag
    nextPageToken: calendars.nextPageToken
    nextSyncToken: calendars.nextSyncToken

  else
   GoogleCalendars.insert
    etag: calendars.etag
    nextPageToken: calendars.nextPageToken
    nextSyncToken: calendars.nextSyncToken

 'tymtu-google-task': () ->
  console.log "Meteor.call('tymtu-google-task')"
  taskLists = GoogleApi.get('tasks/v1/users/@me/lists', {}).items

  if taskLists
   # for each taskList
   _.forEach(taskLists, (taskList, i, a) ->

    tasks = GoogleApi.get(
     "tasks/v1/lists/#{encodeURIComponent(taskList.id)}/tasks"
    ).items

    # for all tasks within said taskList...
    tasks.forEach (taskElement, taskIndex, taskArray) ->

     gTasktOpts = {
      gId: taskElement.id
      displayName: taskElement.title || "--- no title ---"
      endTime: taskElement.due
      tBlob: {}
     }

     Events.upsert {gId: gTasktOpts.gId}, {$set: gTasktOpts }
   )

 'removeEvent': (eId) ->
  Events.remove({_id: eId})
