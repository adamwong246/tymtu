FlowRouter.route '/',
  action: ->
    BlazeLayout.render 'frame'

FlowRouter.route '/events',
  action: ->
    BlazeLayout.render 'frame'

FlowRouter.route '/events/:_id',
  action: ->
    BlazeLayout.render 'frame'

FlowRouter.route '/events/:_id/visualize',
  action: ->
    BlazeLayout.render 'frame'

FlowRouter.route '/secret',
  triggerEnter: ->
    if !Meteor.user()
      if Meteor.loggingIn()
        FlowRouter.go 'index'

FlowRouter.route '/admin',
  triggerEnter: ->
    unless Roles.userIsInRole(Meteor.userId(), ['admin'])
      FlowRouter.go 'index'

FlowRouter.route '/(.*)', ->
  @redirect 'index'
