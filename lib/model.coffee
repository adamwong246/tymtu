@Events = new Mongo.Collection 'events'
@Foci = new Mongo.Collection 'foci'
@GoogleCalendars  = new Mongo.Collection 'googleCalendars'
# @Relations  = new Mongo.Collection 'relations'
@tSchemas  = new Mongo.Collection 'tSchemas'

@Schemas = {}

@Schemas.GoogleCalendars = new SimpleSchema
 userId:
  type: String
  regEx: SimpleSchema.RegEx.Id
  autoValue: ->
   if this.isUpsert || this.isInsert
    Meteor.userId()
 nextSyncToken:
  type: String
 items:
  type: [Object]
  blackbox: true
  optional: true

@Schemas.Foci = new SimpleSchema
 focalPoint:
   type: Date
 tBlob:
  type: Object
  blackbox: true
  optional: true
 startTime:
  type: Date
  optional: true
  autoform:
    afFieldInput:
      type: "bootstrap-datetimepicker"
 endTime:
  type: Date
  optional: true
  autoform:
    afFieldInput:
      type: "bootstrap-datetimepicker"
 eventId:
  type: String
 userId:
  type: String
  regEx: SimpleSchema.RegEx.Id
  autoValue: ->
   if this.isUpsert
    return {$setOnInsert: Meteor.userId()}

# # To ensure a singly directed graph, we must filter through a function which
# # sorts the 2 subjects. It looks up with the first subject as primary and the
# # second subject as secondary.
# @Schemas.Relations = new SimpleSchema
#  primary:
#   type: String
#  secondary:
#   type: String
#  predicate:
#   type: String

@Schemas.Events = new SimpleSchema
 displayName:
  type: String
  optional: true
 gId:
  type: String
  optional: true
 recurringId:
  type: String
  optional: true
 done:
  type: Boolean
  optional: true
 ignored:
  type: Boolean
  optional: true
 recurrence:
  type: [String]
  optional: true
 schema:
  type: Object
  blackbox: true
  defaultValue: {}
 startTime:
  type: Date
  optional: true
 endTime:
  type: Date
  optional: true
 allDay:
  type: Boolean
  optional: true
 userId:
  type: String
  regEx: SimpleSchema.RegEx.Id
  autoValue: ->
   if this.isUpsert || this.isInsert
    Meteor.userId()
 parentId:
  type: String
  optional: true

@Schemas.tSchemas = new SimpleSchema
 displayName:
  type: String
 schemaBlob:
  type: Object

@Schemas.User = new SimpleSchema
  registered_emails: { type: [Object], blackbox: true, optional: true }
  services:
    type: Object
    optional: true
    blackbox: true
  roles:
    type: [ String ]
    optional: true

Events.attachSchema(Schemas.Events)
Foci.attachSchema(Schemas.Foci)
GoogleCalendars.attachSchema(Schemas.GoogleCalendars)
# Relations.attachSchema(Schemas.Relations)
tSchemas.attachSchema(Schemas.tSchemas)
Meteor.users.attachSchema Schemas.User

Events.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    true

Foci.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    true

# Relations.allow
#   insert: (userId, doc) ->
#     true
#   update: (userId, doc) ->
#     true

GoogleCalendars.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    true

tSchemas.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    true

Meteor.users.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    userId == doc._id || Roles.userIsInRole(userId, ['admin'])

# Roles.allow
#   insert: (userId, doc) ->
#     Roles.userIsInRole(userId, ['admin'])
#   update: (userId, doc) ->
#     Roles.userIsInRole(userId, ['admin'])
