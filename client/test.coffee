getRandomArbitrary = (min, max) ->
  Math.random() * (max - min) + min

@loadSomeNewTestData = ()  ->
  Events.insert
    recurrence: []
    displayName: Math.random().toString()
    startTime: moment().add(getRandomArbitrary(-10, 0), 'days').toDate()
    endTime: moment().add(getRandomArbitrary(0, 10), 'days').toDate()

@loadSomeTestData = () ->
 lifeId = Events.insert
  recurrence: []
  displayName: 'Life'
  startTime: moment('06-30-1987').toDate()
  endTime: moment('06-30-1987').add(100, 'years').toDate()

 childhood = Events.insert
  recurrence: []
  displayName: 'childhood'
  parentId:  lifeId
  endTime: moment('06-30-1987').add(21, 'years').toDate()

 retirement = Events.insert
  recurrence: []
  displayName: 'retired'
  parentId:  lifeId
  startTime: moment('06-30-1987').add(75, 'years').toDate()

 career = Events.insert
  recurrence: []
  displayName: 'career'
  parentId: lifeId
  startTime: moment('06-30-1987').add(23, 'years').toDate()
  endTime: moment('06-30-1987').add(75, 'years').toDate()

 chroma = Events.insert
  recurrence: []
  displayName: 'chroma'
  parentId: career
  startTime: moment('02-01-2015').toDate()

 birthdays = Events.insert
  recurrence: []
  displayName: 'birthdays'
  parentId: lifeId

 mybirthday = Events.insert
  recurrence: ["RRULE:#{new RRule({
    freq: RRule.YEARLY,
    interval: 1,
    dtstart: new Date(1987, 6, 30, 10, 30)
  }).toString()}"]
  displayName: 'my birthday'
  parentId: birthdays
  startTime: moment('06-30-1987').toDate()
  startTime: moment('06-31-1987').toDate()
  allDay: true

 cachesBirthday = Events.insert
  recurrence: ["RRULE:#{new RRule({
    freq: RRule.YEARLY,
    interval: 1,
    dtstart: new Date(1980, 8, 29, 10, 30)
  }).toString()}"]

  displayName: "chache's birthday"
  parentId: birthdays
  startTime: moment('08-29-1980').toDate()
  startTime: moment('08-30-1980').toDate()
  allDay: true

  Events.insert
   startTime: moment().add(1, 'day').toDate()
   endTime: moment().add(2, 'day').toDate()
   displayName: "every year"
   recurrence: ['RRULE:FREQ=YEARLY']
   userId: Meteor.userId()
   parentId:  lifeId

  Events.insert
   startTime: moment().add(1, 'day').toDate()
   endTime: moment().add(2, 'day').toDate()
   displayName: "every month"
   recurrence: ['RRULE:FREQ=MONTHLY']
   userId: Meteor.userId()
   parentId:  lifeId

  Events.insert
   startTime: moment().subtract(1, 'month').toDate()
   endTime: moment().subtract(1, 'month').add(1, 'day').toDate()
   displayName: "every day"
   recurrence: ['RRULE:FREQ=DAILY']
   userId: Meteor.userId()
   parentId:  lifeId

  Events.insert
   startTime: moment().endOf('day').toDate()
   endTime: moment().add(1, 'day').endOf('day').toDate()
   recurrence: []
   displayName: "tomorow"
   userId: Meteor.userId()
   parentId:  lifeId

  Events.insert
   startTime: moment().toDate()
   endTime: moment().endOf('week').toDate()
   recurrence: []
   displayName: "rest of week"
   userId: Meteor.userId()
   parentId:  lifeId
