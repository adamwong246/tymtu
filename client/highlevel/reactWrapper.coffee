Template.reactWrapper.helpers
  comp: ->
    FlowRouter.watchPathChange()
    if FlowRouter.current().path == '/'
      mainComp
    else if FlowRouter.current().route.path == '/events'
      eventsComp
    else if FlowRouter.current().route.path == '/events/:_id'
      eventComp
    else if FlowRouter.current().route.path == '/events/:_id/visualize'
      dataVisualizer

  route: -> FlowRouter.current()
