Template.frame.helpers
 alphaUser: ->
  Roles.userIsInRole(Meteor.userId(), 'alpha')

Template.frame.events
 # 'keyup #like': (e) ->
 #  likeQuery.set(document.getElementById("like").value)

 'click #refresh-data-google-calendar': (e) ->
  Meteor.call('tymtu-google-calendar')

Template.main.helpers
 reactiveStyle: ->
  """
  """
 sunStyle: ->
  "background: -webkit-radial-gradient( #{sun().x}px #{sun().y}px, circle, rgba(242,248,247,1) 0%,rgba(249,249,28,1) 3%,rgba(247,214,46,1) 8%, rgba(248,200,95,1) 12%,rgba(201,165,132,1) 30%,rgba(115,130,133,1) 51%,rgba(46,97,122,1) 85%,rgba(24,75,106,1) 100%);"
 sunDayStyle: ->
  """
  opacity: #{1 - sun().y / rwindow.$height()};
  background: -webkit-radial-gradient(#{sun().x}px #{sun().y}px, circle, rgba(252,255,251,0.9) 0%,rgba(253,250,219,0.4) 30%,rgba(226,219,197,0.01) 70%, rgba(226,219,197,0.0) 70%,rgba(201,165,132,0) 100%);
  """
 sunSetStyle: ->
  """
  opacity: #{sun().y / rwindow.$height() - 0.2};
  background: -webkit-radial-gradient(#{sun().x}px #{sun().y}px, circle, rgba(254,255,255,0.8) 5%,rgba(236,255,0,1) 10%,rgba(253,50,41,1) 25%, rgba(243,0,0,1) 40%,rgba(93,0,0,1) 100%);
  """
 skyStyle: ->
  "opacity: #{Math.min(1 - sun().y / rwindow.$height(), 0.99)};"
 darknessOverlaySkyStyle: ->
  "opacity: #{Math.min((sun().y-(rwindow.$height()*7/10)) / (rwindow.$height()-(rwindow.$height()*7/10)), 1)};"
 darknessOverlayStyle: ->
  "opacity: #{Math.min((sun().y - rwindow.$height() / 2) / (rwindow.$height() / 2), 1)};"
 horizonStyle: ->
  if sun().y > rwindow.$height() / 2
   "opacity: #{(rwindow.$height() - sun().y) / (rwindow.$height() / 2) + 0.2};"
  else
   "opacity: #{Math.min(sun().y / (rwindow.$height() / 2), 0.99)};"
 horizonNightStyle: ->
  "opacity: #{(sun().y - rwindow.$height() * 4 / 5) / (rwindow.$height() - rwindow.$height() * 4 / 5)};"
