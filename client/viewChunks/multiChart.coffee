width = 600
height = 400
radius = Math.min(width, height) / 2
margin = {top: 20, right: 20, bottom: 30, left: 50}

@multiChart = React.createClass
  render: ->
    chosenChartType = @props.chartType

    React.DOM.svg {
      width: width
      height: height
    },
      if chosenChartType
        if chosenChartType == 'line'
          React.createElement lineChart, @props
      else
        React.createElement pieChart, @props

@pieChart = React.createClass
  render: ->

    series = @props.series

    uniques = _.chain(series)
    .groupBy( (d) -> d.point )
    .map( (v, k) -> [k, v.length])
    .value()

    arc = d3.svg.arc().outerRadius(radius - 10).innerRadius(0)
    labelArc = d3.svg.arc().outerRadius(radius - 40).innerRadius(radius - 40)
    pie = d3.layout.pie().sort(null).value( (d) -> d[1] )

    piedData = pie(uniques)

    color  = d3.scale.ordinal()
    .range([
      "#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c",
      "#ff8c00"])

    return React.DOM.g {}, _.chain(piedData).map( (d, i) ->
      React.DOM.g {
        className: 'arc'
        transform: "translate( #{width / 2}, #{height / 2} )"
      }, [
        React.DOM.path({
          d: arc(d)
          style:
            fill: color(i)
        })
      ,
        React.DOM.text({
          dy: ".35em"
          transform: "translate(#{labelArc.centroid(d)})"
        }, "#{d.data[0]} * #{d.data[1]}" )
      ]
    ).value()

testData = [
  {focalPoint: moment('24-Apr-07'), point: 93.24}
  {focalPoint: moment('25-Apr-07'), point: 95.35}
  {focalPoint: moment('26-Apr-07'), point: 98.84}
  {focalPoint: moment('27-Apr-07'), point: 99.92}
  {focalPoint: moment('30-Apr-07'), point: 99.80}
]


@lineChart = React.createClass
  render: ->
    series = @props.series
    # series = testData

    console.log series

    x = d3.time.scale()
    .range([0, width])
    .domain(d3.extent(series, (d) -> d.focalPoint ) )

    y = d3.scale.linear()
    .range([height, 0])
    .domain(d3.extent(series, (d) -> d.point ))

    line = d3.svg.line()
    .x( (d) -> x(d.focalPoint) )
    .y( (d) -> y(d.point) )

    React.DOM.g( {},
      React.DOM.path {className: 'line', d: line(series)}
    )
