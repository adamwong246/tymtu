@leftColUl = React.createClass

 render: ->
  props = @props
  state = @state

  depth = props.depth
  head = props.head

  ul {
   key: "mind-map-ul-#{depth}"
  }, _.map props.children, (e) ->
   React.createElement leftColLi, {event: e, head: head, depth: depth + 1}

@leftColLi = React.createClass
 onDragStart: (ev) ->
  ev.stopPropagation()
  ev.dataTransfer.setData("foo", @props.event._id)

 onDrag: (ev) ->
  ev.preventDefault()
  ev.stopPropagation()

 onDragEnd: (ev) ->
  ev.preventDefault()
  ev.stopPropagation()

 onDragEnter: (ev) ->
  ev.preventDefault()
  ev.stopPropagation()

 onDragOver: (ev) ->
  ev.preventDefault()
  ev.stopPropagation()

 onDragLeave: (ev) ->
  ev.preventDefault()
  ev.stopPropagation()

 onDrop: (ev) ->
  ev.preventDefault()
  ev.stopPropagation()
  data = ev.dataTransfer.getData("foo")
  console.log 'onDrop', @props.event._id, data
  Events.update {_id: data}, $set: {parentId: @props.event._id}

 onMouseEnter: (e) ->
  store.dispatch
   type: 'highlightEvent'
   highlightedEventId: @props.event._id

 onMouseLeave: (e) ->
  store.dispatch type: 'clearHighlightEvent'

 render: ->
  event = @props.event

  depth = @props.depth
  head = @props.head

  onDragStart = @onDragStart
  onDrag = @onDrag
  onDragEnd = @onDragEnd
  onDragEnter= @onDragEnter
  onDragOver= @onDragOver
  onDragLeave= @onDragLeave
  onDrop = @onDrop
  onMouseEnter = @onMouseEnter
  onMouseLeave = @onMouseLeave

  if event.instances?.length
   relativePrimeTime = smartRelativeTime _.select(event.instances, (e) -> e.primeness)[0]
  else
   relativePrimeTime = smartRelativeTime event

  li {
   key: "mind-map-li-#{event._id}"

   style:
    border: if head.highlightedEventId == event._id then 'rgba(255,0,0,1) 1px solid' else 'black 0px solid'
    backgroundColor: if head.highlightedEventId == event._id then 'rgba(255,0,0,0.1)' else 'rgba(0,0,255,0.1)'

   draggable: true
   onDragStart: onDragStart
   onDrag: onDrag
   onDragEnd: onDragEnd
   onDragEnter: onDragEnter
   onDragOver: onDragOver
   onDragLeave: onDragLeave
   onDrop: onDrop
   onMouseEnter: onMouseEnter
   onMouseLeave: onMouseLeave
  }, [
   [

    p {className: 'eventPreviewComp'}, [
     b({className: 'display-name'},
      a({href: "/events/#{event._id}"}, "#{event.displayName}, " )
     )

    span({className: 'nice-time'}, relativePrimeTime )

    # if head.highlightedEventId == event._id
    #  React.DOM.span {}, React.createElement verbButtons, {event: event}
    ]

    if event.children
     React.createElement(leftColUl, children: event.children, head: head, depth: depth)
   ]
  ]
