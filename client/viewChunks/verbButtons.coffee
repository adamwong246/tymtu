@verbButtons = React.createClass

 recordOnEvent: () ->
  props = @props
  event = props.event
  _id = event._id

  if event.stateFocus && event.stateFocus.endTime == undefined
   Foci.update _id: event.stateFocus._id,
    $set:
     endTime: new Date()
  else
   Foci.insert
    userId: Meteor.userId()
    focalPoint: event.startTime.toDate()
    eventId: _id
    startTime: new Date()
  # else
  #  Foci.insert
  #   userId: Meteor.userId()
  #   eventId: _id
  #   startTime: new Date()

 recordOffEvent: () ->
  Foci.update {_id: @props.event.stateFocus._id},{$set: endTime: new Date()}

 # doneOffEvent: () ->
 #  Events.update
 #   _id: @props.event._id
 #   ,
 #   $set:
 #    done: false
 #
 # doneOnEvent: () ->
 #  _id = @props.event._id
 #  head = @props.head
 #  # Events.update({_id: _id}, {$set:{done: true}})
 #
 #  if head.event?._id == _id && head.state == _id
 #   Foci.update
 #    _id: head.focus._id
 #    ,
 #    $set:
 #     endTime: new Date()

 # ignoreOffEvent: () ->
 #  _id = @props.event._id
 #  Events.update({_id: _id}, {$set:{ignored: false}})
 #
 # ignoreOnEvent: () ->
 #  _id = @props.event._id
 #  Events.update({_id: _id}, {$set:{ignored: true}})

 playOnEvent:() ->
  playId.set(@props.event.__id)

  # if @props.head.state == 'recording' && @props.head.focus.endTime == undefined
  #  Foci.update {_id: @props.head.focus._id}, {$set: endTime: new Date()}

 playOffEvent:() ->
  playId.set(null)

 isRecording:()->
  #
  # if @props.event.stateFocus
  #  debugger
  #  if @props.event.stateFocus.focalPoint.isSame @props.event.startTime
  #   debugger

  if !@props.event.stateFocus
   false
  else if @props.event.stateFocus.startTime != undefined &&
  @props.event.stateFocus.endTime == undefined &&
  @props.event.stateFocus.focalPoint.isSame @props.event.startTime
   true
  else
   false

 counter:()->@props.event.stateFocus?.startTime.fromNow(true)

 render: ->
  that = this
  props = @props
  event = @props.event
  focusEventId = @props.focus?.eventId
  head = @props.head

  isRecording = @isRecording
  counter = @counter

  div({
   className: 'btn-group btn-group-xs '
   key: "verb-button-#{event._id}"
  },
   (() ->
    buttons = []
    # if nowEvent.recurringId
    #  buttons = buttons.concat a({onClick: that.clickPrev, className: "btn btn-default previous"},  React.DOM.span({className: 'fa fa-step-backward'}) )

    if false # isRecording()
     buttons = buttons.concat a(
      onClick: that.recordOffEvent
      className: "btn btn-default focus-true"
     , React.DOM.span({className: 'fa fa-circle text-danger'}, counter())
     )
    else
     buttons = buttons.concat a(
      onClick: that.recordOnEvent
      className: "btn btn-default focus-false"
     , React.DOM.span({className: 'fa fa-circle'})
     )

    # if false  # _.isEqual(head.state, 'playing') && head.playId == event.__id
    #  buttons = buttons.concat a({onClick: that.playOffEvent, className: "btn btn-success"},  React.DOM.span({className: 'fa fa-play'}))
    # else
    #  buttons = buttons.concat a({onClick: that.playOnEvent, className: "btn btn-default"},  React.DOM.span({className: 'fa fa-play'}))

    if false  # nowEvent.ignored
     buttons = buttons.concat a({onClick: that.ignoreOffEvent, className: "btn btn-warning ignore-true"},  React.DOM.span({className: 'fa fa-pause'}))
    else
     buttons = buttons.concat a({onClick: that.ignoreOnEvent, className: "btn btn-default ignore-false"}, React.DOM.span({className: 'fa fa-pause'}))

    if false  # event.done
     buttons = buttons.concat a({onClick: that.doneOffEvent, className: "btn btn-danger done-true"},  React.DOM.span({className: 'fa fa-stop'}))
    else
     buttons = buttons.concat a({onClick: that.doneOnEvent, className: "btn btn-default done-false"}, React.DOM.span({className: 'fa fa-stop'}))

    # if false # nowEvent.recurringId
    #  buttons = buttons.concat a({onClick: that.clickNext, className: "btn btn-default next"},  React.DOM.span({className: 'fa fa-step-forward'}))

    if event.children
     buttons = buttons.concat a({onClick: that.doneOffEvent, className: "btn btn-default"},  React.DOM.span({className: 'fa fa-compress'}))

     buttons = buttons.concat a({onClick: that.doneOffEvent, className: "btn btn-default"},  React.DOM.span({className: 'fa fa-search-plus'}))

    return buttons
   )()
  )
