@trackingStats = React.createClass
  sumLengthPlanned: (recurrences) ->
    Math.abs(
      _.chain(recurrences)
      .map( (r) ->
        moment(r.startTime).diff(moment(r.endTime), 'seconds')
      ).flatten()
      .reduce(((p, c) -> p + c), 0)
      .value()
    )

  sumLengthFocus: (recurrences) ->

   _.chain(recurrences)
   .map( (r) ->

     _.reduce(r.trackFoci, (memo, element, index) ->
       if element.tBlob.tracking
         memo + moment(r.trackFoci[index+1]?.focalPoint || moment() ).diff(moment(element.focalPoint), 'seconds')
       else
         memo
     , 0)

   ).flatten()
   .reduce(((p, c) -> p + c), 0)
   .value()

  render: ->
    event = @props.event
    instances = if event.instances.length then event.instances else [event]

    console.log instances

    React.DOM.table {className:'table table-bordered'}, [
     React.DOM.tr {}, [
      React.DOM.td {}, "Number of instances"
      React.DOM.td {}, instances.length
     ],
     React.DOM.tr {}, [
      React.DOM.td {}, "Sum of time planned"
      React.DOM.td {}, juration.stringify(@sumLengthPlanned(instances), { format: 'short' })
     ],
     React.DOM.tr {}, [
      React.DOM.td {}, "Sum of time focused"
      React.DOM.td {}, juration.stringify(@sumLengthFocus(instances), { format: 'short' })
     ]
    ]
