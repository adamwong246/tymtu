@eventComp = React.createClass
  mixins: [ ReactMeteorData ]

  getMeteorData: ->
    FlowRouter.watchPathChange()
    getAllTheData({_id: FlowRouter.getParam('_id')})

  deleteEvent: ->
    Meteor.call('removeEvent', FlowRouter.getParam('_id'))
    FlowRouter.go('/events')

  render: ->
    data = @data

    event = data.model.tracks[0]
    console.log event
    unless event
      return React.DOM.span({}, "Error")
    else
      parentId = event?.parentId

      if event.instances?.length
       primeEvent = _.select(event.instances, (e) -> e.primeness)[0]
      else
       primeEvent = event

      if primeEvent.startTime && primeEvent.endTime
       if primeEvent.startTime < new Date() < primeEvent.endTime
        relativePrimeTimeMessage= "ends #{moment(primeEvent.endTime).fromNow()}"
       else if primeEvent.startTime > new Date() && primeEvent.endTime > new Date()
        relativePrimeTimeMessage= "begins #{moment(primeEvent.startTime).fromNow()}"
       else
        relativePrimeTimeMessage= "ended #{moment(primeEvent.endTime).fromNow()}"
      else
       relativePrimeTimeMessage = "whenever"

      div {},
        [
          if parentId
            h5({}, a({href:"/events/#{parentId}"}, "../#{parentId}"))

          React.DOM.a({
            className: 'pull-right', onClick: @deleteEvent
          }, "delete me?")

          h2({className: 'page-header'}, event.displayName)
          p({}, "#{relativePrimeTimeMessage}")

          p({}, "#{primeEvent.startTime.format()} - #{primeEvent.endTime.format()}")

          p({}, "recurrences - #{JSON.stringify(event.recurrence)}")

          React.DOM.hr()
          React.createElement(timeTracker, {event: event})
          React.DOM.hr()
          React.createElement(statusTracker, {event: event})
          React.DOM.hr()

          React.createElement(dataJournal,
           event: event
           tSchemas: data.model?.tSchemas || {}
          )

          React.DOM.p {}, "The Event",
          React.DOM.pre({}, JSON.stringify(event, null, 2))

        ]
