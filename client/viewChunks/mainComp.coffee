@mainComp = React.createClass
 mixins: [ ReactMeteorData ]
 getMeteorData: ->
  getAllTheData()

 render: ->
  data = @data
  page_width = data.view.rightCol.width
  page_height = data.view.rightCol.height
  head = data.head
  scale = head.scale

  nowLine = scale(moment().toDate()) || 0
  children = data.model.tracks

  React.DOM.div({className: 'main-react-comp'},[
   React.createElement(windowDateTimeRanger, {head: data.head})

   React.DOM.div {className: 'row', key: 0},
    React.DOM.div {className: 'col-xs-12', key: 1},

     React.DOM.form {className: 'form-inline', key: 2},

     React.createElement flatFoldToggle, groupBy: data.head.groupBy

      # React.DOM.div {className: 'input-group', key: 3}, [
      #  React.DOM.label {key: 4}, 'group by',
      #  React.DOM.input {type:'text', value: data.head.groupBy, key: 5},
      # ],
      #
      # React.DOM.div {className: 'input-group', key: 6}, [
      #  React.DOM.label {key: 7}, 'sort by',
      #  React.DOM.input {type:'text', value: data.head.sortBy, key: 8},
      # ]

   React.DOM.div {id: 'desktop', key: 9},[
    React.DOM.div {className: 'container', key: 10}
    React.DOM.div( {className: 'left col left-quart', key: 11},
     [
      React.DOM.div( {className: 'body tymtuRow scroll-y', key: 'scroll-y'}, []
       React.createElement leftColUl, {
        head: head
        children: children
        depth: 0
       }
      )
     ]
    )
    React.DOM.div( {className: 'right col', key: 'right-col'},
     [
      React.DOM.svg({
       key: 'the-svg'
       id: 'the-svg'
       width: '100%'
       height: '100%'
       viewBox: "0 0 #{page_width} #{page_height}"
      }, [
       React.DOM.rect(
        key:"past-box"
        id: "past-box"
        x: 0
        width: nowLine
        y: 0
        height: "100%"
       ),
       React.createElement(timeMap, {
        head: head
        scale: scale
        children: children
        xOrigin: 0
        yOrigin: 0
        xBound: page_width
        yBound: page_height
        depth: 0
       }),
       React.DOM.line(
        key: "now-line"
        id: "now-line"
        x1: nowLine
        x2: nowLine
        y1: 0
        y2: "100%"
        stroke: 'red'
       )
      ]
      )
     ]
    )
   ]
  ])
