theblob = (event) ->
  event.trackFoci.slice(-1)[0]?.tBlob

@statusTracker = React.createClass

  render: ->
    event = @props.event

    React.DOM.div({className: "status-tracker"},
      if theblob(event)?.statusDone == true
        React.createElement(statusTrackerNotDone, {event: event})
      else
        React.createElement(statusTrackerDone, {event: event})
    )

@statusTrackerDone = React.createClass
  onClick: ->
    newBlob = EJSON.clone(theblob(@props.event)) || {}
    newBlob.statusDone = true

    Foci.insert
      eventId: @props.event._id
      focalPoint: new Date()
      tBlob: newBlob
      userId: Meteor.userId()

  render: ->
    React.DOM.div({
      className: "status-tracker-off"},
      React.DOM.a({
        className: "status-tracking-affirm"
        onClick: @onClick
      }, "done?")
    )

@statusTrackerNotDone = React.createClass
  onClick: ->
    newBlob = EJSON.clone(theblob(@props.event)) || {}
    newBlob.statusDone = false
    console.log newBlob

    Foci.insert
      eventId: @props.event._id
      focalPoint: new Date()
      tBlob: newBlob
      userId: Meteor.userId()

  render: ->
    React.DOM.div({className: "status-tracker-on"},[

      React.DOM.a({
        className: "status-tracking-unaffirm"
        onClick: @onClick
      }, "not done?")
    ])
