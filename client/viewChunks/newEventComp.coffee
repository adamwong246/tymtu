@newEventComp = React.createClass
 setRRule: (e) ->
  @setState newEventRRule: e.target.value

 setDisplayName: (e) ->
  @setState newEventDisplayName: e.target.value

 setStartTime: (date) ->
  @setState newEventStartTime: date

 setEndTime: (date) ->
  @setState newEventEndTime: date

 createEvent: (e) ->
  e.preventDefault()

  attributes =
   displayName: @state.newEventDisplayName
   startTime: new Date @state.newEventStartTime
   endTime: new Date @state.newEventEndTime
   recurrence: [@state.newEventRRule]

  console.log attributes

  Events.insert attributes

 render: ->
  state = @state
  setDisplayName = @setDisplayName
  setStartTime = @setStartTime
  setEndTime = @setEndTime
  createEvent = @createEvent
  setRRule = @setRRule

  React.DOM.form {},[
   React.DOM.p {}, 'displayName'
   React.DOM.input
    type:'text'
    onChange:setDisplayName
    value:state?.newEventDisplayName
   React.DOM.p {}, 'startTime'
   React.createElement(magicDateTimePicker, {value: state?.newEventStartTime, callback: setStartTime})
   React.DOM.p {}, 'endTime'
   React.createElement(magicDateTimePicker, {value: state?.newEventEndTime, callback: setEndTime})

   React.DOM.p {}, 'rrule'
   React.DOM.a {href: 'http://jkbrzt.github.io/rrule/'}, "* hint *"
   React.DOM.input
    type: 'text'
    onChange: setRRule
    value: state?.newEventRRule

   React.DOM.button {type:'submit', onClick: createEvent}, 'new event'
  ]
