@dataVisualizer = React.createClass
  mixins: [ ReactMeteorData ]

  getMeteorData: ->
    FlowRouter.watchPathChange()
    getAllTheData({_id: FlowRouter.getParam('_id')})

  render: ->
    data = @data

    event = data.model.tracks[0]

    unless event
      return React.DOM.span({}, "Error")
    else
      parentId = event?.parentId

      keyPath = FlowRouter.current().queryParams.keyPath

      series = _.chain(event.trackFoci)
      .select( (f) ->
        f.tBlob[keyPath] != undefined
      ).map( (f) ->
        _id: f._id
        focalPoint: f.focalPoint
        point: f.tBlob[keyPath] || 0
      ).sortBy( (a, b) ->
        a.focalPoint > b.focalPoint
      ).value()

      chartType = event.schema.properties?[keyPath]?.chartType

      div {}, [
        h2({className: 'page-header'}, [
          event.displayName,

          React.DOM.span({className: 'small'}, keyPath )
        ]),

        React.createElement multiChart, {series: series, chartType: chartType}

        if keyPath == 'tracking'
          React.createElement trackingStats, {event: event}

        h4 {}, "data series"

        pre {}, JSON.stringify( series, null, 2)
      ]
