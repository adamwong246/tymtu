@dataJournal = React.createClass

  getModel: -> @props.event.trackFoci.slice(-1)[0]?.tBlob || {}

  getSchema: -> @props.event.schema || {}

  changeModel: (model) ->
    console.log "changeModel: ", model
    Foci.insert
      eventId: @props.event._id
      focalPoint: new Date()
      tBlob: model
      userId: Meteor.userId()

  changeSchema: (schema) ->
    console.log "changeSchema: ", schema
    Events.update {_id: @props.event._id}, {$set: {'schema': schema}}

  render: ->
    props = @props
    event = props.event
    tSchemas = props.tSchemas || []
    id = event._id
    model = @getModel()
    schema = @getSchema()

    changeModel = @changeModel
    changeSchema = @changeSchema

    div({className: 'data-journal'}, [

     form( {},
      [
       React.createElement(tTreeJsonEditor, {
        id: id
        model: model
        schema: schema
        tSchemas: tSchemas
        backCall: changeModel
       }),

       h3({}, "model"),
       React.createElement(jsonTextEditor, {
        blob: model
        backCall: changeModel
       }),

       h3({}, "schema"),
       React.createElement(jsonTextEditor, {
        blob: schema
        backCall: changeSchema
       }),

       # pre {}, JSON.stringify(model, null, 2)
       # pre {}, JSON.stringify(schema, null, 2)
      ])

     h4 {}, "extracted data"

     pre {}, JSON.stringify( _.chain(event.trackFoci).map( (f) ->
      _id: f._id
      focalPoint: f.focalPoint
      tBlob: f.tBlob
     ).value(), null, 2
     )

    ])
