@jsonTextEditor = React.createClass
 stringify:(string) -> JSON.stringify(string, null, 2)

 getInitialState: ->
  text: @stringify(@props.blob)
  cache: EJSON.clone(@props.blob)

 componentWillReceiveProps: (newProps) ->
  if newProps.blob
   if !_.isEqual(newProps.blob, @state.cache)
    @setState(text: @stringify(newProps.blob))
    @setState(cache: EJSON.clone(newProps.blob))

 onChangeBlob: (e) ->
  @setState(text: e.target.value)

 onBlurBlob: (e) ->
  try
   theJson = JSON.parse(@state.text)
   @props.backCall(theJson)
  catch error
   console.log("failed to save model. #{error}")

 render: ->
  props = @props
  state = @state
  onChangeBlob = @onChangeBlob
  onBlurBlob = @onBlurBlob
  text = state.text

  textarea
   className: "form-control" # make the textarea very wide
   rows: 10
   onChange: onChangeBlob
   onBlur: onBlurBlob
   value: text
