@bud = React.createClass
 getDefaultProps:()->
  theValue: ""

 onChangeValue: (e) ->
  @props.backCall {theKey: @props.theKey, theValue: e} # e

 onChangeKey: (e) ->
  @props.backCall {theKey: e, theValue: @props.theValue}

 onChangeType: (theType) ->
  console.log "onChangeType: (#{theType})"
  console.log @props.theKey
  console.log tTypeDefault(theType)
  @props.backCall {theKey: @props.theKey, theValue: tTypeDefault(theType)}

 onDelete: (e) ->
  @props.backCall({theKey: null, theValue: null})
  return false

 render: ->
  tSchemas = @props.tSchemas
  React.DOM.span({className: "bud"}, [

   React.createElement leafType,
    theType: tTypeof @props.theValue
    backCall: @onChangeType
    tSchemas: tSchemas

   React.createElement leafValue,
    theValue: @props.theValue
    backCall: @onChangeValue
    tSchemas: tSchemas
  ])
