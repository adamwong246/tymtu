@leafValue = React.createClass
 render:->
  span className:"leaf-value",
  switch tTypeof @props.theValue
   when 'string'  then React.createElement tipValueString,  {theValue: @props.theValue, backCall: @props.backCall, tSchemas: @props.tSchemas}
   when 'number'  then React.createElement tipValueNumber,  {theValue: @props.theValue, backCall: @props.backCall, tSchemas: @props.tSchemas}
   when 'boolean' then React.createElement tipValueBoolean, {theValue: @props.theValue, backCall: @props.backCall, tSchemas: @props.tSchemas}
   when 'array'   then React.createElement tipValueArray,   {theValue: @props.theValue, backCall: @props.backCall, tSchemas: @props.tSchemas}
   when 'object'  then React.createElement tipValueObject,  {theValue: @props.theValue, backCall: @props.backCall, tSchemas: @props.tSchemas}
   else React.DOM.span({}, "??? #{@props.theValue}")

# @leafValueUnfocused = React.createClass
#  render:->
#   props = @props
#
#   React.DOM.span className:"tipValueUnvosudes",
#   switch tTypeof @props.theValue
#    when 'string'  then React.DOM.span {backCall: @props.backCall}, @props.theValue
#    when 'number'  then React.DOM.span {backCall: @props.backCall}, @props.theValue
#    when 'boolean'  then React.DOM.span {backCall: @props.backCall}, @props.theValue
#
#    # when 'array'   then React.createElement listBranch,   {theValue: @props.theValue, backCall: @props.backCall}
#    # when 'object'  then React.createElement dictBranch,  {theValue: @props.theValue, backCall: @props.backCall}
#
#    # when 'array'   then React.createElement twigArray,   {theValue: @props.theValue, backCall: @props.backCall}
#    # when 'object'  then React.createElement twigObject,  {theValue: @props.theValue, backCall: @props.backCall}
#
#    when 'array'
#     React.DOM.ul {}, _.map(@props.theValue, (e) ->
#      React.DOM.li {}, React.createElement(leafValueUnfocused, {theValue: e})
#     )
#
#    when 'object'
#     React.DOM.ul {}, _.map(Object.keys(@props.theValue), (e) ->
#      React.DOM.li {}, React.createElement(leafValueUnfocused, {theValue: e})
#     )
#
#    else React.DOM.span({}, "??? #{@props.theValue}")

@leafKey = React.createClass
 getInitialState:->
  theKey: @props.theKey
  # cachedKey: EJSON.clone(@props.theKey)

 componentWillReceiveProps: (newProps) ->
  if newProps.theKey
   @setState theKey: newProps.theKey
  # # console.log "leafKey.componentWillReceiveProps "
  # # console.log "old"
  # # console.log @state.cachedKey
  # # console.log "new"
  # # console.log newProps.theKey
  #
  # if !_.isEqual(newProps.theKey, @state.cachedKey)
  #  # newProps.theKey &&
  #  # newProps.theKey != @props.theKey &&
  #  # @state.cachedKey &&
  #  # console.log "existing key found: #{newProps.theKey} != #{@state.cachedKey} . updating"
  #  @setState(theKey: newProps.theKey)
  #  @setState(cachedKey: EJSON.clone(newProps.theKey))

 onChange:(e)->
  @setState theKey: e.target.value

 onBlur:(e)->
  @setState theKey: e.target.value
  # @setState(cachedKey: e.target.value)
  @props.backCall e.target.value

 render:->
  React.DOM.input({
   className: "leafKey"
   type: "text"
   value: @state.theKey
   onChange: @onChange
   onBlur: @onBlur
  }, {})

@leafType = React.createClass
 getInitialState: ->
  theType: @props.theType

 componentWillReceiveProps:(p)->
  @setState({theType: p.theType})

 onChangeType: (e) ->
  @setState {theType: e.target.value}
  @props.backCall e.target.value

 theTypes: () ->
  @props.tSchemas

 render: ->
  React.DOM.select({className: "leafType", value: @props.theType, onChange: @onChangeType}, _.map @theTypes(), (e) ->
   # React.DOM.option {value: e}, e
   switch tTypeof e
    when 'string' then React.DOM.option {value: e}, e
    when 'object' then React.DOM.option {value: e.title}, e.title
    else React.DOM.span({}, "??? #{@props.theValue}")
  )

@leafDelete = React.createClass
 onClick:(e)->
  @props.backCall()

 render:->
  React.DOM.button {className: "leafDelete", onClick: @onClick},
   React.DOM.i {className:"fa fa-close"}, {}

@leafChart = React.createClass
 onClick:(e)->
  alert @props.mPath
  false

 render:->
  a {href: "/events/#{@props.id}/visualize?keyPath=#{encodeURIComponent(@props.mPath)}", className: "leafChart" },
   i {className:"fa fa-bar-chart "}, {}
