@dictBranch = React.createClass
  backCall: (incomingChanges, prevKey) ->
    updatedDict = @props.blob

    if prevKey != incomingChanges.theKey
      delete updatedDict[prevKey]

    updatedDict[incomingChanges.theKey || prevKey] = incomingChanges.theValue

    @props.backCall updatedDict

  render: () ->
    props = @props
    id = @props.id
    blob = @props.blob || {}
    schema = @props.schema || {}
    tSchemas = @props.tSchemas
    backCall = @backCall

    ul({},
      _.chain(Object.keys(blob).sort()).map( (e, i, a) ->
        theKey = e
        if props.mPath == ""
          mPath = e
        else
          mPath = "#{props.mPath}.#{e}"

        li({},
          React.createElement(dictElem, {
            id: id
            theKey: e,
            theValue: blob[e],
            theSchema: schema.properties?[e]
            tSchemas: tSchemas
            mPath: mPath
            backCall: (state) ->
              backCall(state, @theKey)
          })
        )
      ).value().concat(
        li({className: 'shiny'},
          React.createElement dictElem,
            id: @props.id
            backCall: backCall
            tSchemas: tSchemas
         )
      )
    )

@dictElem = React.createClass
  getDefaultProps:->
    theValue: ""
  onChangeValue: (e) ->
    @props.backCall e
  onChangeKey: (e) ->
    @props.backCall {theKey: e, theValue: @props.theValue}, @props.theKey
  onChangeType: (e) ->
    @props.backCall {theKey: @props.theKey, theValue: tTypeDefault(e)}
  onDelete: (e) ->
    @props.backCall({theKey: null, theValue: null})
    false

  render: ->
    tSchemas = @props.tSchemas

    unless @props.theSchema

      span({className: "dictElem"}, [

       React.createElement(leafDelete, {backCall: @onDelete}),

       React.createElement(leafKey,
         {theKey: @props.theKey, backCall: @onChangeKey}),

       React.createElement(bud, {
        theType: tTypeof(@props.theValue)
        tSchemas: tSchemas
        theValue: @props.theValue
        backCall: @onChangeValue
        })

       React.createElement(leafChart, {id: @props.id, mPath: @props.mPath})
      ])
    else
      span({className: "dictElem"}, [
        span({}, @props.theKey)
        React.createElement(leafValue,
          {theValue: @props.theValue, backCall: @onChangeValue})
        React.createElement(leafChart, {id: @props.id, mPath: @props.mPath})
      ])
