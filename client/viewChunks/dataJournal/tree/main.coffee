# tree - the whole thing
# trunk - the entry point
# branch - dictionaries and lists. Later, more complex colletions
# twig
# leaf - values
# tip - type specific input elements

@tTreeJsonEditor = React.createClass
  render: ->
    React.createElement tTrunk, @props

@tTrunk = React.createClass

  render: ->
    if tTypeof(@props.model) == 'object'
      comp = dictBranch
    else if tTypeof(@props.model) == 'array'
      comp = listBranch

    schemas = ["string", "number", "boolean","array", "object"]
    .concat(@props.tSchemas)

    div {},
      React.createElement comp,
       id: @props.id
       backCall: @props.backCall
       blob: @props.model
       schema: @props.schema
       tSchemas: schemas
       mPath: ""

@tTypeof = (thing) ->
  t = typeof thing
  unless t == "object"
    t
  else
    if _.isArray(thing)
      "array"
    else
      "object"

@tTypeDefault = (theType) ->
  if theType == "string"
    ""
  else if theType == "number"
    0
  else if theType == "boolean"
    true
  else if theType == "array"
    []
  else if theType == "object"
    {}
  else
    throw 'wtf'
