@listBranch = React.createClass
 backCall: (incomingChanges, prevIndex) ->

  k = incomingChanges.theIndex || prevIndex
  v = incomingChanges.theValue
  updatedList = @props.blob || []

  if v == null
   updatedList.splice(prevIndex, 1)
  else
   if k != undefined
    updatedList[k] = v
   else if k == undefined
    updatedList.push(v)

  console.log "listBranch.backCall-ing: ", updatedList
  @props.backCall(updatedList)

 render: () ->
  @props = props
  blob = @props.blob || {}
  schema = @props.schema || {}
  tSchemas = @props.tSchemas
  backCall = @backCall

  ol({},
   _.chain(
    Object.keys(blob).sort()
   ).map( (e, i, a) ->
    theKey = e

    if props.mPath == ""
     mPath = e
    else
     mPath = "#{props.mPath}.#{i}"


    li({},
     React.createElement(listElem, {
      theValue: blob[e],
      theIndex: i,
      theSchema: schema.properties?[e]
      tSchemas: tSchemas
      mPath: mPath
      backCall: (state) ->
       backCall(state, @theKey)
     })
    )
   ).value().concat(
    li({className: 'shiny'},React.createElement(listElem,{
     backCall: backCall
     tSchemas: tSchemas
    }))
   )
  )

@listElem = React.createClass
 getDefaultProps:()->
  theValue: ""
 onChangeValue: (e) ->
  @props.backCall {theIndex: @props.theIndex, theValue: e.theValue}
 onChangeType: (e) ->
  @props.backCall {theValue: tTypeDefault(e)}
 onDelete: (e) ->
  @props.backCall ({theValue: null})
  return false

 render: ->
  tSchemas = @props.tSchemas

  unless @props.theSchema
   React.DOM.span({className: "listElem"}, [
    React.createElement(leafDelete, {backCall: @onDelete})
    React.createElement(leafChart, {mPath: @props.mPath})

    React.createElement(bud, {
     theType: tTypeof(@props.theValue)
     tSchemas: tSchemas
     backCall: @onChangeType
     theValue: @props.theValue
     backCall: @onChangeValue
     })
   ])
  else
   React.DOM.span({className: "listElem"}, [
    React.DOM.span({}, "#{@props.theKey}: ")
    React.createElement(leafValue, {theValue: @props.theValue, backCall: @onChangeValue})
   ])
