@tipValueString = React.createClass

 getInitialState: -> theValue: @props.theValue

 componentWillReceiveProps: (p) ->
  @setState({theValue: p.theValue})

 onChange: (e) ->
  @setState({theValue: e.target.value})

 onBlur:(e)->
  @setState({theValue: e.target.value})
  @props.backCall(e.target.value)

 onKeyPress:(e)->
  if e.charCode == 13
   @setState({theValue: e.target.value})
   @props.backCall(e.target.value)

 render:->
  React.DOM.input({
   className: "tipValueString"
   type: "text"
   value: @state.theValue
   onChange: @onChange
   onBlur: @onBlur
   onKeyPress: @onKeyPress
  }, {})

@tipValueNumber = React.createClass
 getInitialState:->
  theValue: @props.theValue
 componentWillReceiveProps:(p)->
  @setState({theValue: p.theValue})

 onChange:(e)->
  @setState({ theValue: e.target.valueAsNumber })
 onBlur:(e)->
  @setState({theValue: e.target.valueAsNumber})
  @props.backCall(e.target.valueAsNumber)
 onKeyPress:(e)->
  if e.charCode == 13
   @setState({theValue: e.target.valueAsNumber})
   @props.backCall(e.target.valueAsNumber)

 render:->
  React.DOM.input({
   className: "tipValueNumber"
   type: "number"
   value: @state.theValue
   onChange: @onChange
   onBlur: @onBlur
   onKeyPress: @onKeyPress
  }, {})

@tipValueBoolean = React.createClass
 getInitialState:->
  theValue: @props.theValue
 componentWillReceiveProps:(p)->
  @setState({theValue: p.theValue})

 onChange:(e)->
  @setState({ theValue: e.target.checked })
  @props.backCall(e.target.checked)

 render:->
  React.DOM.input({
   className: "tipValueBoolean"
   type: "checkbox"
   checked: @state.theValue
   onChange: @onChange
  }, {})

@tipValueArray = React.createClass
 getInitialState:->
  theValue: @props.theValue
 componentWillReceiveProps:(p)->
  @setState({theValue: p.theValue})

 render:->
  React.DOM.span({className: "tipValueArray"}
   React.createElement(listBranch, {
    backCall: @props.backCall
    blob: @props.theValue
    tSchemas: @props.tSchemas
   })
  )

@tipValueObject = React.createClass
 getInitialState:->
  theValue: @props.theValue
 componentWillReceiveProps:(p)->
  @setState({theValue: p.theValue})

 render:->
  React.DOM.span({className: "tipValueObject"},
   React.createElement(dictBranch, {
    backCall: @props.backCall
    blob: @state.theValue
    tSchemas: @props.tSchemas
   })
  )
