theblob = (event) ->
  event.trackFoci.slice(-1)[0]?.tBlob

@timeTracker = React.createClass

  render: ->
    event = @props.event

    React.DOM.div({className: "time-tracker"},
      if theblob(event)?.tracking
        React.createElement(timeTrackerOn, {event: event})
      else
        React.createElement(timeTrackerOff, {event: event})
    )

@timeTrackerOff = React.createClass
  onClick: ->
    newBlob = EJSON.clone(theblob(@props.event)) || {}
    newBlob.tracking = true

    Foci.insert
      eventId: @props.event._id
      focalPoint: new Date()
      tBlob: newBlob
      userId: Meteor.userId()

  render: ->
    React.DOM.div({
      className: "time-tracker-off"},
      React.DOM.a({
        className: "start-tracking"
        onClick: @onClick
      }, "start tracking?")
    )

@timeTrackerOn = React.createClass
  onClick: ->
    newBlob = EJSON.clone(theblob(@props.event)) || {}
    newBlob.tracking = false
    console.log newBlob

    Foci.insert
      eventId: @props.event._id
      focalPoint: new Date()
      tBlob: newBlob
      userId: Meteor.userId()

  render: ->
    humanizedDuration = moment.duration(moment() - @props.event.trackFoci.slice(-1)[0]?.focalPoint).humanize()

    React.DOM.div({className: "time-tracker-on"},[

      React.DOM.span({}, "tracking since #{humanizedDuration} ago... " ),

      React.DOM.a({
        className: "end-tracking"
        onClick: @onClick
      }, "end tracking?")
    ])
