@timeMapChunk = React.createClass
 onmouseover: ->
  console.log @props.child._id
  store.dispatch
   type: 'highlightEvent'
   highlightedEventId: @props.child._id

 render: ->
  onmouseover = @onmouseover
  props = @props

  child = props.child
  scale = props.scale
  trackHeight = props.trackHeight
  index = props.index
  head = props.head
  yOrigin = props.yOrigin
  depth = props.depth

  xStart = (scale(child.startTime) || props.xOrigin)
  xEnd = (scale(child.endTime) || props.xBound)

  y = trackHeight * index + props.yOrigin

  React.DOM.g {
   onMouseEnter: onmouseover
   }, [
   React.DOM.rect({
    key: "track-shadow"
    x: props.xOrigin
    y: y
    width: props.xBound
    height: trackHeight
    fill: if head.highlightedEventId == child._id then 'rgba(255,0,0,0.1)' else 'transparent'
   }),

   unless child.instances
    [
     React.DOM.rect({
      key: "rect-#{child._id}"
      x: xStart
      y: y
      width: xEnd - xStart
      height: trackHeight
      stroke: if head.highlightedEventId == child._id then 'red' else 'blue'
      strokeWidth: '1px'
      fill: if head.highlightedEventId == child._id then 'rgba(255,0,0,0.1)' else 'rgba(0,0,255,0.1)'
     })
    ]

   else
    g {className: 'instances'}, _.map child.instances, (childInstance, childIndex) ->
     isDone = childInstance.stateFocus?.tBlob?.statusDone

     [
      React.DOM.rect({
       'data-id': "#{childInstance._id} #{childInstance.__id}"
       key: "#{childInstance._id} #{childInstance.__id}"
       x: (scale(childInstance.startTime) || props.xOrigin)
       y: y
       width: (scale(childInstance.endTime) || props.xBound) - (scale(childInstance.startTime) || props.xOrigin)
       height: trackHeight
       stroke: if head.highlightedEventId == childInstance._id then 'red' else 'blue'
       strokeWidth: '1px'
       fill: if isDone then 'black' else 'rgba(0,0,255,0.1)'
      })

     ].concat(
      _.map child.trackFoci, (f) ->
       React.DOM.circle({
        cx: (scale(f.focalPoint) || props.xOrigin)
        cy: y + ( trackHeight / 2)
        r: 3
        stroke: 'orange'
        fill: 'yellow'
        strokeWidth: '1px'
       })
     )

   if child.children then React.createElement(timeMap, {
    scale: scale
    children: child.children
    xOrigin: xStart
    yOrigin: trackHeight * index + yOrigin
    yBound: trackHeight * ( index + 1) + yOrigin
    xBound: xEnd
    head: head
    depth: depth + 1
   })
  ]
