@eventsComp = React.createClass
 mixins: [ReactMeteorData]

 getMeteorData: ->
  events: Events.find().fetch()

 render: ->
  data = @data

  React.DOM.div {}, [
   React.createElement newEventComp
   React.DOM.hr {},
   React.DOM.h2 {}, 'all events for all time.'
   React.DOM.ul {}, _.map data.events, (e) ->
    React.DOM.li {}, [
     React.DOM.a({href:"/events/#{e._id}"}, "#{e.displayName}, #{e._id}")
    ]
  ]
