@magicDateTime = React.createClass

 getInitialState:->
  {collapsed: true}

 expandIt:()->
  @setState({collapsed: false})

 collpaseIt:()->
  @setState({collapsed: true})

 callback: (dateTime) ->
  @props.callback(dateTime)

 render:->
  props = @props
  state = @state
  dateTime = props.dateTime
  collapsed = state.collapsed

  expandIt = @expandIt
  collpaseIt = @collpaseIt
  callback = @callback

  unless collapsed
   contents = React.DOM.div({className:''}, [
    React.DOM.a({href: '#', onClick: collpaseIt}, 'X')
    React.createElement(magicDateTimePicker, {dateTime: dateTime, callback: callback})
   ])
  else
   contents = React.DOM.a({href: '#', onClick: expandIt}, moment(dateTime).format("YYYY.MMM.dd, Do.H:mm") )

  React.DOM.div({className: 'magic-date-time'}, contents )
