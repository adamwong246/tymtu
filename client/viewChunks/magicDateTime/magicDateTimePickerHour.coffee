@magicDateTimePickerHour = React.createClass
 getInitialState: () ->
  {
   hour: moment(@props.dateTime).hour()
   _stash: @props.dateTime
  }

 componentWillReceiveProps: (newProps) ->
  if newProps.dateTime
   if !_.isEqual( moment(newProps.dateTime).hour(), moment(@state._stash.dateTime).hour() )
    @setState(hour: moment(newProps.dateTime).hour())
    @setState(_stash: newProps.dateTime)

 hours:()->
  _.map [1..24], (e) -> e

 onChange: (e) ->
  @setState({hour: event.target.value})
  @props.callback(parseInt(event.target.value))

 render:->
  props = @props
  state = @state
  onChange = @onChange
  hours = @hours

  React.DOM.span({className: 'magic-date-picker-hour'},
   React.DOM.select({
    value: state?.hour
    onChange: onChange
   },
    _.map(hours(), (key) ->
     React.DOM.option({value: key}, key)
    )
   )
  )
