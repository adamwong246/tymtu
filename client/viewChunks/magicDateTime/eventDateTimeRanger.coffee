@eventDateTimeRanger = React.createClass

 getInitialState:->
  {collapsed: true}

 smartRelativeTime: (o) ->
  if o.startTime && o.endTime
   if o.startTime < new Date() < o.endTime
    "ends #{moment(o.endTime).fromNow()}"
   else if o.startTime > new Date() && o.endTime > new Date()
    "begins #{moment(o.startTime).fromNow()}"
   else
    "ended #{moment(o.endTime).fromNow()}"

 expandIt:()->
  @setState({collapsed: false})

 collpaseIt:()->
  @setState({collapsed: true})

 render: ->
  props = @props
  state = @state
  event = props.event
  collapsed = state.collapsed
  smartRelativeTime = @smartRelativeTime
  expandIt = @expandIt
  collpaseIt = @collpaseIt

  unless collapsed
   contents = React.DOM.div({className:'well-sm'}, [
    React.DOM.a({onClick: collpaseIt}, 'close')
    React.createElement(magicDateTime, {dateTime: event.startTime})
    React.createElement(magicDateTime, {dateTime: event.endTime})
   ])
  else
   contents = React.DOM.a({className: 'nice-time', onClick: expandIt}, smartRelativeTime(event))

  React.DOM.div({className: 'magic-date-range'}, contents )
