@magicDateTimePickerYear = React.createClass
 getInitialState: () ->
  {
   year: moment(@props.dateTime).year()
   _stash: @props.dateTime
  }

 componentWillReceiveProps: (newProps) ->
  if newProps.dateTime
   if !_.isEqual( moment(newProps.dateTime).year(), moment(@state._stash.dateTime).year() )
    @setState(year: moment(newProps.dateTime).year())
    @setState(_stash: newProps.dateTime)

 years:()->
  _.map [minDate.years()..maxDate.years()], (e) -> e

 onChange: (e) ->
  @setState({inspectedBlobKey: event.target.value})
  @props.callback(parseInt(event.target.value))

 render:->
  props = @props
  state = @state
  dateTime = moment(props.dateTime).format()
  onChange = @onChange
  years = @years

  React.DOM.span({className: 'magic-date-picker-year'}, [
   React.DOM.select({
    value: state?.year
    onChange: onChange
   },
    _.map(years(), (key) ->
     React.DOM.option({value: key}, key)
    )
   )
  ])
