@windowDateTimeRanger = React.createClass

 setFrom:(dateTime) ->
  store.dispatch
   type: 'setStartDateRange'
   dateRangerStartTime: dateTime

 setTo:(dateTime) ->
  store.dispatch
   type: 'setEndDateRange'
   dateRangerEndTime: dateTime

 render: ->
  props = @props
  state = @state
  setFrom = @setFrom
  setTo = @setTo
  humanScale = props.head.humanScale
  start = props.head.ranger.start
  end = props.head.ranger.end

  React.DOM.div {id: 'window-date-time-picker'},
   React.DOM.div {className: 'row'},
    React.DOM.div {className: 'col-xs-5'},
     React.createElement( magicDateTime, {callback: setFrom, dateTime: start })
    React.DOM.div {className: 'col-xs-2 text-center'},[
     React.DOM.p({}, moment(end).from(start, true))
     # React.DOM.p({}, "scale: #{humanScale}" )
    ]
    React.DOM.div {className: 'col-xs-5 text-right'},
     React.createElement( magicDateTime, {callback: setTo, dateTime: end })
