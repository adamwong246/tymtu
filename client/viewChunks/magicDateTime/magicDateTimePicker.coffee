@magicDateTimePicker = React.createClass
 getInitialState: () ->
  {
   dateTime: @props.dateTime
   _stash: @props.dateTime
  }

 componentWillReceiveProps: (newProps) ->
  if newProps.dateTime
   if !_.isEqual( newProps.dateTime, @state._stash.dateTime )
    @setState(dateTime: newProps.dateTime)
    @setState(_stash: newProps.dateTime)

 callbackYear: (year) ->
  @props.callback moment(@state.dateTime).year(year)

 callbackMonth: (month) ->
  @props.callback moment(@state.dateTime).month(month)

 callbackDate: (date) ->
  @props.callback moment(@state.dateTime).date(date)

 callbackHour: (hour) ->
  @props.callback moment(@state.dateTime).hour(hour)

 callbackMinute: (minute) ->
  @props.callback moment(@state.dateTime).minute(minute)

 render:->
  props = @props
  state = @state
  dateTime = state.dateTime

  callbackYear   = @callbackYear
  callbackMonth  = @callbackMonth
  callbackDate    = @callbackDate
  callbackHour   = @callbackHour
  callbackMinute = @callbackMinute

  React.DOM.span({className: 'magic-date-picker'},[
   React.createElement(magicDateTimePickerYear,   {dateTime: dateTime, callback: callbackYear})
   React.createElement(magicDateTimePickerMonth,  {dateTime: dateTime, callback: callbackMonth})
   React.createElement(magicDateTimePickerDate,    {dateTime: dateTime, callback: callbackDate})
   React.createElement(magicDateTimePickerHour,   {dateTime: dateTime, callback: callbackHour})
   React.createElement(magicDateTimePickerMinute, {dateTime: dateTime, callback: callbackMinute})
  ])
