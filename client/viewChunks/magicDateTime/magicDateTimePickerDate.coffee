@magicDateTimePickerDate = React.createClass
 getInitialState: () ->
  {
   date: moment(@props.dateTime).date()
   _stash: @props.dateTime
  }

 componentWillReceiveProps: (newProps) ->
  if newProps.dateTime
   if !_.isEqual( moment(newProps.dateTime).date(), moment(@state._stash.dateTime).date() )
    @setState(date: moment(newProps.dateTime).date())
    @setState(_stash: newProps.dateTime)

 dates:()->
  _.map [1..31], (e) -> e

 onChange: (e) ->
  @setState({date: event.target.value})
  @props.callback(parseInt(event.target.value))

 render:->
  props = @props
  state = @state

  onChange = @onChange
  dates = @dates

  React.DOM.span({className: 'magic-date-picker-date'},
   React.DOM.select({
    value: state?.date
    onChange: onChange
   },
    _.map(dates(), (key) ->
     React.DOM.option({value: key}, key)
    )
   )
  )
