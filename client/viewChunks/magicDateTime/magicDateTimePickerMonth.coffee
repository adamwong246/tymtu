@magicDateTimePickerMonth = React.createClass
 getInitialState: () ->
  {
   month: moment(@props.dateTime).month()
   _stash: @props.dateTime
  }

 componentWillReceiveProps: (newProps) ->
  if newProps.dateTime
   if !_.isEqual( moment(newProps.dateTime).month(), moment(@state._stash.dateTime).month() )
    @setState(month: moment(newProps.dateTime).month())
    @setState(_stash: newProps.dateTime)

 months:()->
  _.map [0..11], (e) -> e

 onChange: (e) ->
  @setState({month: event.target.value})
  @props.callback(parseInt(event.target.value))

 render:->
  props = @props
  state = @state
  onChange = @onChange
  months = @months

  React.DOM.span({className: 'magic-date-picker-month'},
   React.DOM.select({
    value: state?.month
    onChange: onChange
   },
    _.map(months(), (key) ->
     React.DOM.option({value: key}, key)
    )
   )
  )
