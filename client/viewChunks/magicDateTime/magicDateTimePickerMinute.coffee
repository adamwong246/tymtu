@magicDateTimePickerMinute = React.createClass
 getInitialState: () ->
  {
   minute: moment(@props.dateTime).minute()
   _stash: @props.dateTime
  }

 componentWillReceiveProps: (newProps) ->
  if newProps.dateTime
   if !_.isEqual( moment(newProps.dateTime).minute(), moment(@state._stash.dateTime).minute() )
    @setState(minute: moment(newProps.dateTime).minute())
    @setState(_stash: newProps.dateTime)

 minutes:()->
  _.map [0..59], (e) -> e

 onChange: (e) ->
  @setState({minute: event.target.value})
  @props.callback(parseInt(event.target.value))

 render:->
  props = @props
  state = @state
  onChange = @onChange
  minutes = @minutes

  React.DOM.span({className: 'magic-date-picker-minute'},
   React.DOM.select({
    value: state?.minute
    onChange: onChange
   },
    _.map(minutes(), (key) ->
     React.DOM.option({value: key}, key)
    )
   )
  )
