@flatFoldToggle = React.createClass

  ungroupClick: ->
    store.dispatch({type: "setGroupBy", groupBy: null})

  groupClick: ->
    store.dispatch({type: "setGroupBy", groupBy: "parentId"})

  render: ->
    groupBy = @props.groupBy

    if groupBy is 'parentId'
      React.DOM.a({
       onClick: @ungroupClick
       href: "#{}"}, "grouped"
      )
    else
      React.DOM.a({
       onClick: @groupClick
       href: "#{}"}, "ungrouped"
      )
