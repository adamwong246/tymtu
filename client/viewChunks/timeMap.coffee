@timeMap = React.createClass

 render: ->
  props = @props

  children = props.children
  scale = props.scale
  depth = props.depth
  head = props.head

  xOrigin = props.xOrigin # + 1
  yOrigin = props.yOrigin # + 1
  xBound  = props.xBound  # - 1
  yBound  = props.yBound  # - 1

  if children

   React.DOM.g({className: 'children'}, _.map children, (child, index) ->
    React.DOM.g {
     className: 'child'
     'data-id': "#{child._id} #{child.__id}"
     key: "group-#{child._id}"
    },
     React.createElement timeMapChunk, {
      child: child
      trackHeight: (yBound - yOrigin) / children.length
      xOrigin: xOrigin
      yOrigin: yOrigin
      xBound: xBound
      yBound: yBound
      scale: scale
      index: index
      head: head
      depth: depth

     }
   )
