Accounts.ui.config
 requestPermissions:
  google: [
   'https://www.googleapis.com/auth/calendar'
   'https://www.googleapis.com/auth/tasks'
   'https://www.googleapis.com/auth/userinfo.email'
  ]
 requestOfflineToken:
  google: true

Meteor.subscribe 'events'
Meteor.subscribe 'foci'
Meteor.subscribe 'googleCalendars'
Meteor.subscribe 'tSchemas'
Meteor.subscribe 'allUsers'

SimpleSchema.debug = true

Handlebars.registerHelper 'dump', ->
 JSON.stringify(this, null, 2)

@minDate = moment().subtract 2, 'years'
@maxDate = moment().add 2, 'years'

@reactiveNow = new ReactiveVar(new Date)
Meteor.setInterval (->
 reactiveNow.set new Date
 return
), 10000

@likeQuery = new ReactiveVar("")

@smartRelativeTime = (o) ->
 if o.startTime && o.endTime
  if o.startTime < new Date() < o.endTime
   "ends #{moment(o.endTime).fromNow()}"
  else if o.startTime > new Date() && o.endTime > new Date()
   "begins #{moment(o.startTime).fromNow()}"
  else
   "ended #{moment(o.endTime).fromNow()}"

primeEvent = (group) -> _.select(group, (e) -> e.primeness)[0]

primeMultiSorter = (sorter) ->
 (a, b) ->
  if a.instances?.length > 0
   aPrime = primeEvent(a.instances)
  else
   aPrime = a

  if b.instances?.length > 0
   bPrime = primeEvent(b.instances)
  else
   bPrime = b

  if sorter == 'priority'

   aStart = aPrime.startTime
   bStart = bPrime.startTime
   aEnd = aPrime.endTime
   bEnd = bPrime.endTime

   rtr = (start, end) ->

    if start < moment() && moment() < end
     return 0
    else if start > moment()
     return -1
    else if moment() > end
     return -2
    else
     return -3

   aRtr = rtr aStart, aEnd
   bRtr = rtr bStart, bEnd

   if aRtr == bRtr

    if aRtr == 0
     return aEnd - bEnd
    else if aRtr == -1
     return aStart - bStart
    else if aRtr == -2
     return bEnd - aEnd
    else
     return 0

   else if aRtr < bRtr
    return 1
   else if aRtr > bRtr
    return -1
   else
    return 0

  else if sorter == 'startTime'
   aPrime.startTime - bPrime.startTime
  else
   aPrime.displayName.length - bPrime.displayName.length

@store = Redux.createStore (state, action) ->

 # trigger an update
 reactiveNow.set new Date()

 switch action.type
  when 'setInspectedElement'
   state.inspectedElementId = action.inspectedElementId
   state
  when 'setStartDateRange'
   state.ranger.start = action.dateRangerStartTime
   state
  when 'setEndDateRange'
   state.ranger.end = action.dateRangerEndTime
   state
  when 'highlightEvent'
   state.highlightedEventId = action.highlightedEventId
   state
  when 'clearHighlightEvent'
   state.highlightedEventId = null
   state
  when 'setGroupBy'
   state.groupBy = action.groupBy
   state
  when 'setSortBy'
   state.sortBy = action.sortBy
   state
  else
   state
, {
 humanScale: RRule.SECONDLY
 sortBy: 'priority'
 groupBy: 'parentId'
 ranger:
  start: minDate # moment().add(-1, 'day')
  end: maxDate # moment().add(7, 'day')
}

@getAllTheData = (q) ->
 eventHandle = Meteor.subscribe 'events'
 fociHandle  = Meteor.subscribe 'foci'

 likeQueryRegEx = new RegExp(".*#{likeQuery.get()}.*", "i")

 storeState = store.getState()
 ranger = storeState.ranger
 @startDatetime = ranger.start.toDate()
 @endDatetime = ranger.end.toDate()

 # timeScale = ""
 # if rangerEnd.diff(rangerStart, 'hours') < 24
 #  timeScale = 'hours'
 # else if rangerEnd.diff(rangerStart, 'hours') > 24 && rangerEnd.diff(rangerStart, 'days') < 7
 #  timeScale = 'days'
 # else if rangerEnd.diff(rangerStart, 'days') > 7 && rangerEnd.diff(rangerStart, 'weeks') < 4
 #  timeScale = 'weeks'
 # else if rangerEnd.diff(rangerStart, 'weeks') > 4 && rangerEnd.diff(rangerStart, 'months') < 12
 #  timeScale = 'months'
 # else if rangerEnd.diff(rangerStart, 'months') > 12
 #  timeScale = 'years'
 # else
 #  timeScale = 'wtf'

 # Get the focuses and curent foci, if it exists
 ##############################################
 foci = _.chain(Foci.find({}, {$sort: {startTime: 1}}).fetch())
 .map((e)->
  # convert start and end times to moments
  if e.startTime
   e.startTime = moment e.startTime
  if e.endTime
   e.endTime = moment e.endTime
  if e.focalPoint
   e.focalPoint = moment e.focalPoint
  e
 ).value()

 # setup the big query
 ##############################################
 query = q || $and:[
   $or:[
     {$and:[{ startTime: $gt: ranger.start }, { startTime: $lt: ranger.end }]},
     {$and:[{ endTime:   $gt: ranger.start }, { endTime:   $lt: ranger.end }]},
     {$and:[{ startTime: $lt: ranger.start }, { endTime:   $gt: ranger.end }]}
     {$or:[{ startTime: $exists: false }, { endTime:   $exists: false }]}
     {recurrence: {$exists: true}}
   ],
   {'displayName': likeQueryRegEx}
 ]

 # get all the events and process them into a list
 ##############################################
 tracks = _.chain(
  Events.find(query, {$sort: {startTime: 1}}).fetch()

 ).map((e) -> # if there are recurence rules, parse them into 'instances'

  # convert start and end times to moments
  if e.startTime
   e.startTime = moment e.startTime

  if e.endTime
   e.endTime = moment e.endTime

  if e.recurrence?.length > 0
   # debugger if e.displayName == "chache's birthday"
   e.rRuleSet = new RRuleSet(true)

   rules = _.chain(e.recurrence)
   .select( (r)->
    r?[0..4] == "RRULE"
   ).value()

   _.each rules, (r) ->
    slice = r[6..-1]
    s = moment(e.startTime).toDate()
    rOpts = new RRule.fromString(slice).options
    rOpts.dtstart = s
    e.rRuleSet.rrule new RRule(rOpts)
    e.rRuleSet.rdate s

   e.rRuleExpansions = e.rRuleSet.between(ranger.start.toDate(), ranger.end.toDate())

   # if there are any expanded dates, expand into a list of pseudo-events
   e.instances = _.chain(e.rRuleExpansions).map( (date, i, a) ->
    mOrig = moment(e.startTime)

    if e.allDay
     newStart = moment(date).startOf("day")
     newEnd = moment(newStart).endOf("day")
    else
     newStart = moment(date)
     .hours(mOrig.hours())
     .minutes(mOrig.minutes())
     .seconds(mOrig.seconds())
     .millisecond(mOrig.millisecond())
     .clone()
     newEnd = moment(newStart).add((moment(e.endTime).diff(mOrig)/2), 'ms').clone()

    {
     displayName: e.displayName
     __id: e._id + i
     _id: e._id
     startTime: newStart
     endTime: newEnd
     primeness: false
    }

   ).value()

  # find all the foci for this track
  e.trackFoci = _.chain(foci)
  .select((f)->
   f.eventId == e._id
  ).sort((a, b)->
   a.startTime < b.startTime
  ).value()

  # stateFocus:
  # set the state of each event to the previous focus, thus giving it a state
  # if there are any instances, set for each
  if e.instances?.length
   _.chain(e.instances)
   .map( (instance) ->

    instance.stateFocus = _.chain(e.trackFoci)
    .select( (f) ->
     f.focalPoint.isBefore(instance.endTime) && f.focalPoint.isAfter(instance.startTime)
    ).sort( (a, b) ->
     if a.focalPoint.isBefore b.focalPoint
      1
     else if b.focalPoint.isBefore a.focalPoint
      -1
     else
      0
    ).first()
    .value()

    return instance
   ).value()
  else
   e.stateFocus = _.chain(e.trackFoci)
   .select((f) -> f.focalPoint < moment(e.endTime) )
   .sort((a, b)-> a.focalPoint <  b.focalPoint)
   .first().value()

  # if there are any instances, mark the prime element
  if e.instances?.length
   group = e.instances

   if group.length == 1
    group[0].primeness = true

   else
    now = moment().toDate()

    nowElement = _.chain(group)
    .select((e, i, a) ->
     e.startTime < now && e.endTime > now
    ).sortBy((a, b)->
     b.startTime - a.startTime
    ).first()
    .value()

    if nowElement
      nowElement.primeness = true
    else
     thenElement = _.chain(group)
     .select((e, i, a) ->
      e.startTime > now
     ).sortBy((a, b)->
      a.startTime - b.startTime
     ).first()
     .value()

     if thenElement
      thenElement.primeness = true
     else
      prevElement = _.chain(group)
      .select((e, i, a) ->
       e.endTime < now
      ).sortBy((a, b)->
       b.endTime - a.endTime
      ).first()
      .value()

      if prevElement
       prevElement.primeness = true
      else
       group[0].primeness = true

  e
 ).value()

 grouper = storeState.groupBy
 sorter = storeState.sortBy

 if grouper != null

  # http://jsfiddle.net/LkkwH/1/
  unflatten = (array, parent, tree, grouper, sorter) ->
   tree = if typeof tree != 'undefined' then tree else []
   parent = if typeof parent != 'undefined' then parent else _id: undefined

   children = _.chain(array)
   .filter((child) -> child[grouper] == parent._id)
   .sort(primeMultiSorter(sorter))
   .value()

   if !_.isEmpty(children)
    if parent._id == undefined
     tree = children
    else
     parent['children'] = children
    _.each children, (child) ->
     unflatten array, child, undefined, grouper #, sorter
     return
   return tree

  orgedTracks = unflatten(tracks, undefined, undefined, grouper) # , sorter)

 else
  orgedTracks = _.chain(tracks)
  .sort(primeMultiSorter(sorter))
  .value()

 box = $('div.right.col')

 # organize all the data to be passed to view
 ##############################################
 {
  currentUser: Meteor.user()
  handlesLoading: !eventHandle.ready() && !fociHandle.ready()

  head:
   highlightedEventId: storeState.highlightedEventId
   groupBy: storeState.groupBy
   sortBy: storeState.sortBy
   theNow: reactiveNow.get()
   ranger: ranger
   scale:  d3.time.scale()
    .clamp(false)
    .domain([ ranger.start, ranger.end])
    .range([0, box.width()])

  model:
   tracks: orgedTracks
   foci: foci
   tSchemas: tSchemas.find().fetch()

  view:
   rightCol:
    width: box.width()
    height: box.height()
   rwindow: rwindow

 }
