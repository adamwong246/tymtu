Template.admin.helpers
 users: ->
  Meteor.users.find()

Template.admin.events
 'click .make-alpha': (e) ->
  Roles.addUsersToRoles(@_id, 'alpha')
