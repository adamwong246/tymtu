Template.tutorial.helpers
 lifeEvent: () ->
  {displayName: "Your life"}

 yearsOld: () ->
  s = AutoForm.getFieldValue('startTime', 'birthTutorialForm')
  e = AutoForm.getFieldValue('endTime', 'birthTutorialForm')

  if s && e
   moment(e).from(moment(s), true)
  else
   "?"

AutoForm.hooks
 birthTutorialForm:
  onSuccess: (formType, result) ->
   Router.go('/')
