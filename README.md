# README
## tymtu

adam wong, adamwong246@gmail.com, 2016

a combination of workflowy, calendar, and time trackers

### features

- break your time down hierarchically
- expand recurring events into event instances
- record notes and user attention tracking
- expose statistics with graphs
